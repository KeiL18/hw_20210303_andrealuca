package com.classi;

public class Prodotto {

	private int id;
	private String nome;
	private String codice;
	private float prezzo;
	private float quantita;
	
	public Prodotto(){
		
	}
	public Prodotto(int varID, String varNome, String varCodice, float varPrezzo, float varQta){
		this.setId(varID);
		this.setNome(varNome);
		this.setCodice(varCodice);
		this.setPrezzo(varPrezzo);
		this.setQuantita(varQta);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}
	public float getQuantita() {
		return quantita;
	}
	public void setQuantita(float quantita) {
		this.quantita = quantita;
	}
}
