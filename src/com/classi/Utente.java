package com.classi;

public class Utente {

	private int id;
	private String username;
	private String password;
	private String ruolo;
	
	public Utente(){
		
	}
	public Utente(int varID, String varUser, String varPass, String varRuolo){
		this.setId(varID);
		this.setUsername(varUser);
		this.setPassword(varPass);
		this.setRuolo(varRuolo);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRuolo() {
		return ruolo;
	}
	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	
	
}
