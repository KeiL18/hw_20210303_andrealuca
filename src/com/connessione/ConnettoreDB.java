package com.connessione;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnettoreDB {

	private final String host = "127.0.0.1";
	private final int port = 3306;
	private final String user = "root";
	private final String pass = "toor";
	private final String db = "lez_27_gestione_prodotti";
	
	private Connection conn;
	private static ConnettoreDB ogg_connettore;
	
	public static ConnettoreDB getInstance() {
		if(ogg_connettore == null) {
			ogg_connettore = new ConnettoreDB();
		}
		return ogg_connettore;
	}
	
	public Connection getConnection() throws SQLException {
		if(conn == null) {
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName(host);
			dataSource.setPortNumber(port);
			dataSource.setUser(user);
			dataSource.setPassword(pass);
			dataSource.setUseSSL(false); 
			dataSource.setDatabaseName(db);
			
			conn = dataSource.getConnection();
		}
		return conn;
	}
}

