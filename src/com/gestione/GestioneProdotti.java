package com.gestione;

import java.util.ArrayList;

import com.classi.Prodotto;
import com.connessione.ConnettoreDB;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GestioneProdotti {

//	private ArrayList<Prodotto> elencoProdotti = new ArrayList<Prodotto>();
	
	public ArrayList<Prodotto> generaElenco() throws SQLException{
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
		
		Connection conn = ConnettoreDB.getInstance().getConnection();
		
		String query = "SELECT id, nome, codice, prezzo, quantita FROM prodotto";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
		
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Prodotto prodTemp = new Prodotto();
			prodTemp.setId(risultato.getInt(1));
			prodTemp.setNome(risultato.getString(2));
			prodTemp.setCodice(risultato.getString(3));
			prodTemp.setPrezzo(risultato.getFloat(4));
			prodTemp.setQuantita(risultato.getFloat(5));
			
			elenco.add(prodTemp);
		}
		
		return elenco;
	}
	
	public boolean aggiungiProdotto(String varNome, String varCodice, String varPrezzo, String varQta) throws SQLException {
		Connection conn = ConnettoreDB.getInstance().getConnection();
		
		String query = "INSERT INTO Prodotto(nome, codice, prezzo, quantita) VALUE (?, ?, ?, ?)";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, varNome);
		ps.setString(2, varCodice);
		ps.setFloat(3, Float.parseFloat(varPrezzo));
		ps.setFloat(4, Float.parseFloat(varQta));
		
		if(ps.executeUpdate() > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean eliminaProdotto(String varID) throws SQLException {
		Connection conn = ConnettoreDB.getInstance().getConnection();
		
		String query = "DELETE FROM prodotto WHERE id = ?";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setInt(1, Integer.parseInt(varID));
		
		if(ps.executeUpdate() > 0) {
			return true;
		}
		else {
			return false;
		}
	}
}
