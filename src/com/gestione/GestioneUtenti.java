package com.gestione;

import java.util.ArrayList;

import com.classi.Utente;
import com.connessione.ConnettoreDB;
import com.mysql.jdbc.PreparedStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GestioneUtenti {
	
		public ArrayList<Utente> generaElencoUtenti() throws SQLException{
			ArrayList<Utente> elenco = new ArrayList<Utente>();
			
			Connection conn = ConnettoreDB.getInstance().getConnection();
			
			String query = "SELECT username, pass, ruolo FROM Utente";
			PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query);
			
			ResultSet risultato = ps.executeQuery();
			
			while(risultato.next()) {
				Utente userTemp = new Utente();
				userTemp.setUsername(risultato.getString(1));
				userTemp.setPassword(risultato.getString(2));
				userTemp.setRuolo(risultato.getString(3));
				
				elenco.add(userTemp);
			}
			
			return elenco;
		}
		
}
