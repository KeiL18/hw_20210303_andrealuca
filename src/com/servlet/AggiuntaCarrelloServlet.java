package com.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gestione.GestioneProdotti;

public class AggiuntaCarrelloServlet extends HttpServlet{

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
		HttpSession sessione = request.getSession();
		
		if(sessione.getAttribute("isAuth") != null && (boolean)sessione.getAttribute("isAuth") == true) {
			String varCodice = request.getParameter("codice_prod");
			
			String prodotti = sessione.getAttribute("prodotti").toString();
			if(prodotti == "") {
				prodotti = varCodice;
			}
			else {
				prodotti += "," + varCodice;
			}
			sessione.setAttribute("prodotti", prodotti);
			response.sendRedirect("Home.jsp");
		}
		else
			response.sendRedirect("LogIn.html");
	}
}
