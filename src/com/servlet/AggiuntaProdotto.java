package com.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gestione.GestioneProdotti;

public class AggiuntaProdotto extends HttpServlet{

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String varNome = request.getParameter("input_nome");
		String varCodice = request.getParameter("input_codice");
		String varPrezzo = request.getParameter("input_prezzo");
		String varQta = request.getParameter("input_qta");
		
		GestioneProdotti gestore = new GestioneProdotti();
		try {
			if(gestore.aggiungiProdotto(varNome, varCodice, varPrezzo, varQta)) {
				response.sendRedirect("gestioneEcommerce.jsp");
			}
			else {
				response.sendRedirect("ERRquery.html");
			}
				
		} catch (SQLException e) {
			response.sendRedirect("ERRquery.html");
		}
	}
}
