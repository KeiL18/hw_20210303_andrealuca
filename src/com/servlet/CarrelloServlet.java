package com.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CarrelloServlet extends HttpServlet{
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		HttpSession sessione = request.getSession();
		
		if(sessione.getAttribute("isAuth") != null && (boolean)sessione.getAttribute("isAuth") == true) {
			response.sendRedirect("carrello.jsp");
		}
		else{
			response.sendRedirect("LogIn.html");
		}
	}
}
