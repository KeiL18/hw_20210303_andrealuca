package com.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gestione.GestioneProdotti;

public class EliminaProdottoServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String varID = request.getParameter("id_prodotto");
		
		GestioneProdotti gestore = new GestioneProdotti();
		try {
			if(gestore.eliminaProdotto(varID)) {
				response.sendRedirect("gestioneEcommerce.jsp");
			}
			else {
				response.sendRedirect("ERRquery.html");
			}
				
		} catch (SQLException e) {
			response.sendRedirect("ERRquery.html");
		}
	}

}
