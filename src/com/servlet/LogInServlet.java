package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.classi.Utente;
import com.gestione.GestioneUtenti;

public class LogInServlet extends HttpServlet{
	
	private GestioneUtenti gestore = new GestioneUtenti();
	private ArrayList<Utente> elencoUtenti = new ArrayList<Utente>();
	
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String var_user = request.getParameter("input_username");
		String var_pass = request.getParameter("input_password");
		
		try{
			elencoUtenti = gestore.generaElencoUtenti();
		}catch (SQLException e) {
			response.sendRedirect("ERRaccessoDB.html");
		}
		
		HttpSession sessione = request.getSession();
		
		if(sessione.getAttribute("isAuth") != null && (boolean)sessione.getAttribute("isAuth") == true) {
			if(sessione.getAttribute("user").equals(var_user)) {
				switch((sessione.getAttribute("ruolo")).toString()) {
					case "ADMIN":
						response.sendRedirect("gestioneEcommerce.jsp");
						return;
					case "USER":
						//TODO: mandare lo USER alla sua pagina
						break;
				}
			}
			
		}
		
		for(int i = 0; i < elencoUtenti.size(); i++) {
			Utente varUtente = elencoUtenti.get(i);
			if (varUtente.getUsername().equals(var_user)) {
				if(varUtente.getPassword().equals(var_pass)) {
					sessione.setAttribute("isAuth", true);
					sessione.setAttribute("user", var_user);
					switch(varUtente.getRuolo()) {
					case "ADMIN":
						sessione.setAttribute("ruolo", "ADMIN");
						response.sendRedirect("gestioneEcommerce.jsp"); 
						return;
					case "USER":
						sessione.setAttribute("ruolo", "USER");
						//TODO: mandare lo USER alla sua pagina
						break;
					}
				}
				else {
					response.sendRedirect("ERRcredenzialiAccesso.html");
				}
			}
			else {
				response.sendRedirect("ERRcredenzialiAccesso.html");
			}
		}
		
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		out.print("NON PUOI ACCEDERE A QUESTA FUNZIONE!");
	}
}
