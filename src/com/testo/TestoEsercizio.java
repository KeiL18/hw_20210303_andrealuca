package com.testo;

public class TestoEsercizio {
	/*
	 * PROGETTO PERSONALE:

	Fase 1:
	Creare un sistema di gestione di prodotti, ogni prodotto � caratterizzato da:
	Nome del prodotto
	Codice
	Prezzo
	Quantit� disponibile
	
	L�elenco dei prodotti sar� consultabile liberamente senza autenticazione, invece le operazioni di Creazione, Modifica ed Eliminazione sono prerogativa di un 
	utente autenticato come amministratore (all�inizio ignorare il ruolo e pensare solo all�utente autenticato).
	Sul database sono presenti pi� credenziali utente che permettono di modificare i prodotti.
	
	/// ///
	
	Fase 2:
	Permettere l'autenticazione di un utente semplice (che si differenzi dall�amministratore) e alla consultazione dell�elenco di prodotti permettere la 
	composizione di un carrello. Questo sar� consultabile in un�apposita pagina dedicata.
	Ogni elemento prima dell�inserimento nel carrello dovr� subire un controllo sulla quantit� del prodotto.
	Ad ogni apertura della pagina del carrello verr� effettuato un controllo su tutte le quantit� e se c�� discrepanza con la quantit� disponibile visualizzare 
	un avviso.
	 */
}
